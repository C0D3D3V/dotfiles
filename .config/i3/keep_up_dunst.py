#!/usr/bin/env python

import os
import time

from datetime import datetime

import psutil


def check_if_process_is_running(process_name):
    for proc in psutil.process_iter():
        try:
            if process_name.lower() in proc.name().lower():
                return True
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    return False


skript_started_at = datetime.now()

LOG_PATH = '~/.dunst_log.log'
full_log_path = os.path.expanduser(LOG_PATH)
if os.path.isfile(full_log_path):
    log_file_size = os.path.getsize(full_log_path)
    if log_file_size > 20000:
        os.remove(full_log_path)

counter = 0
just_started = False
while True:
    if not check_if_process_is_running('dunst'):
        counter += 1
        with open(full_log_path, 'a+', encoding='utf-8') as fs:
            fs.write(
                f'Attempt {counter} of starting dunst at: {datetime.now()} (Script started at {skript_started_at})\n'
            )
        os.system(f"dunst --config ~/.config/dunst/dunstrc >> {LOG_PATH} 2>&1 &")
        just_started = True
        time.sleep(0.1)
    elif just_started:
        os.system(
            f"notify-send 'Attempt {counter} of starting dunst at: {datetime.now()} (Script started at {skript_started_at})' -t 3000 &"
        )
        just_started = False
    else:
        time.sleep(3)
